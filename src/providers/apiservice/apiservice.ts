import { Injectable } from '@angular/core';
import { Http } from  '@angular/http';
import 'rxjs/add/operator/map';
/*
  Generated class for the ApiserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiserviceProvider {
  public data: any;
  private http: any;
  public status: any;

  constructor(http: Http) {
    this.http = http;
  }
  getData(URL) {
      // console.log('get',URL);
      // console.log(this.http.get(`${URL}`).map((respond) => respond.json()));
      return this.http.get(`${URL}`).map((respond) => respond.json());
  }

  postData(URL){
      // console.log(URL);
      // console.log(this.http.post(`${URI}`).map((respond) => respond.json()));
      return this.http.post(`${URL}`).map((result) => result.json());
  }


}
