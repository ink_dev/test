import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BarcodeScanner } from '@ionic-native/barcode-scanner'
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from  '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CheckpointPage } from '../pages/checkpoint/checkpoint';
import { RedeemPage } from '../pages/redeem/redeem';
import { ApiserviceProvider } from '../providers/apiservice/apiservice';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CheckpointPage,
    RedeemPage,
  ],
  imports: [
    BrowserModule,    
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CheckpointPage,
    RedeemPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiserviceProvider
  ]
})
export class AppModule {}
