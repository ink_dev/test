import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner ,BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { AlertController} from 'ionic-angular';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';

/**
 * Generated class for the CheckpointPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkpoint',
  templateUrl: 'checkpoint.html',
})
export class CheckpointPage {
  options :BarcodeScannerOptions;
  cptype: number;

  choice = [
    '',
    'OPTICAL 4.0',
    'FRAME STATION',
    'DIGITAL STATION',
    'REDEEM CODE',
  ]

  qrcodeID: string;  
  linkAPI : string;  
  name: string;
  comp_name: string;
  obj_data: object;
  obj_status: object;

  tmp_cp: number;
  success = false;
  updateAPI : string;

  process_running = false;
  text_update: string;
  text_choice: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private BarcodeScanner : BarcodeScanner,
    public api: ApiserviceProvider,) {
      this.text_choice = '';
      this.cptype = this.navParams.get('checkpoint');
      // console.log(this.choice)
      this.text_choice = this.choice[this.cptype];
      console.log(this.text_choice)

  }

  scanQRcode() {
    this.process_running = true;
    this.tmp_cp = 0;
    this.success = false;
    this.options =  {
      preferFrontCamera:true
    }

    this.BarcodeScanner.scan(this.options).then ((barcodeData) => {
      this.qrcodeID = barcodeData.text;
      this.process_running = false;
      this.linkAPI = "http://varpevent.com/e/2018-essilor/api/getdata/" + barcodeData.text;
      if(this.qrcodeID == "") {
        // alert('QR CODE INCORRECT');
      } else {

        this.api.getData(this.linkAPI)
        .subscribe(res => {
          this.obj_data = res.data;
          if(res.data !== "false") {
            this.name = this.obj_data[0].full_name;
            this.comp_name = this.obj_data[0].comp_name;
            console.log(this.name,this.comp_name);
            if ( this.cptype == 1) {
              this.tmp_cp = this.obj_data[0].p1;
            } else if (this.cptype == 2) {
              this.tmp_cp = this.obj_data[0].p2;
            } else if (this.cptype == 3) {
              this.tmp_cp = this.obj_data[0].p3;
            }
              this.updateData(this.qrcodeID,this.cptype);
          } else {
            alert('No data ไม่พบข้อมูล');
          }
        }, error => {
          console.log(error);
  
        });
      }
    
    },(err) => {
      alert(err);
      console.log('error', err);
    });
  }

  testapi() {
    this.tmp_cp = 0;
    const p = 1;
    const qr = 'ESL0001'

      this.linkAPI = "http://varpevent.com/e/2018-essilor/api/getdata/" + qr;
      if(this.qrcodeID == "") {
        alert('QR CODE INCORRECT');
      } else {

        this.api.getData(this.linkAPI)
        .subscribe(res => {

          this.obj_data = res.data;
          if(res.data !== "false") {
            this.name = this.obj_data[0].full_name;
            this.comp_name = this.obj_data[0].comp_name;
            // console.log(this.name,this.comp_name);
            if ( p == 1) {
              this.tmp_cp = this.obj_data[0].p1;
            } else if (p == 2) {
              this.tmp_cp = this.obj_data[0].p2;
            } else if (p == 3) {
              this.tmp_cp = this.obj_data[0].p3;
            }
            console.log(this.tmp_cp)
            this.updateData(qr,p);
          } else {
            alert('no data');
          }
        }, error => {
          console.log(error);
  
        });
      }
  }

  updateData(qrcode,p){
    this.success = true;
    if (this.tmp_cp == 0) {
      this.updateAPI = "http://varpevent.com/e/2018-essilor/api/updatedata/" + qrcode + '/p' + p;
      this.api.postData(this.updateAPI)
      .subscribe(res => {
        this.text_update = 'CHECK IN SUCCESS';
        // this.obj_status = res.status;
  
        // if(this.obj_status[0].api == "ok") {
        // this.success = true;
        //   alert('success');
        // } else {
        //   alert('fail');
        // }

        let TIME_IN_MS = 7000;
        let hideTimeout = setTimeout( () => {
        // this.navCtrl.popToRoot();
          this.backToscan();
        }, TIME_IN_MS);
       })
    } else {
      this.text_update = 'ALREADY CHECK IN';
      // alert('You have checked in at this point')
    }    
  }

  presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Login',
      inputs: [
        {
          name: 'password',
          placeholder: 'Password',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Login',
          handler: data => {
            if (data.password == 'warp#event1234') {
              // logged in!
              this.navCtrl.popToRoot();
              
            } else {
              // invalid login
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  backToscan() {
    this.success = false;
  }

  backToHome() {
    this.navCtrl.popToRoot();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckpointPage');
  }

}
