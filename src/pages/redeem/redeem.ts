import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner ,BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import { AlertController} from 'ionic-angular';

/**
 * Generated class for the RedeemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-redeem',
  templateUrl: 'redeem.html',
})
export class RedeemPage {
  options :BarcodeScannerOptions;
  qrcodeID: string;
  linkAPI : string;  
  name: string;
  comp_name: string;
  obj_data: object;
  obj_redeem: object;

  status_p1: string;
  status_p2: string;
  status_p3: string;
  status_re: string;

  updateAPI : string;

  success = false;
  view_redeem = false;
  submit_redeem = false;
  process_running = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public AlertController: AlertController,
    private BarcodeScanner : BarcodeScanner,
    public api: ApiserviceProvider) {
  }


  scanQRcode() {
    this.process_running = true;
    this.submit_redeem = false;
    this.options =  {
      preferFrontCamera:true
    }

    this.BarcodeScanner.scan(this.options).then ((barcodeData) => {
      this.process_running = false;
      this.qrcodeID = barcodeData.text;
      this.linkAPI = "http://varpevent.com/e/2018-essilor/api/getdata/" + barcodeData.text;
      if(this.qrcodeID == "") {
        // alert('QR CODE INCORRECT');
      } else {

        this.api.getData(this.linkAPI)
        .subscribe(res => {

          this.obj_data = res.data;
          if(res.data !== "false") {
            this.view_redeem = true;
            this.name = this.obj_data[0].full_name;
            this.comp_name = this.obj_data[0].comp_name;
            this.status_p1 = this.obj_data[0].p1;
            this.status_p2 = this.obj_data[0].p2;
            this.status_p3 = this.obj_data[0].p3;
            this.status_re = this.obj_data[0].status_redeem;

            if( this.status_p1 == '1'  && this.status_p2 == '1'  && this.status_p3 == '1' && this.status_re == '0'  ) {
              this.submit_redeem = true;
            }

          } else {
            alert('No data ไม่พบข้อมูล');
          }
        }, error => {
          alert(error);
          console.log(error);
        });
      }
    
    },(err) => {
      alert(err);
      console.log('error', err);
    });
  }

  testapi() {
    this.submit_redeem = false;
    const qr = 'ESL0001'
      this.qrcodeID = qr;
      this.linkAPI = "http://varpevent.com/e/2018-essilor/api/getdata/" + qr;
      if(this.qrcodeID == "") {
        alert('QR CODE INCORRECT');
      } else {

        this.api.getData(this.linkAPI)
        .subscribe(res => {

          this.obj_data = res.data;

          if(res.data !== "false") {
            this.view_redeem = true;
            console.log(this.obj_data[0].p1,this.obj_data[0].p2,this.obj_data[0].p3,this.obj_data[0].status_redeem);
            this.name = this.obj_data[0].full_name;
            this.comp_name = this.obj_data[0].comp_name;
            console.log(this.comp_name)
            this.status_p1 = this.obj_data[0].p1;
            this.status_p2 = this.obj_data[0].p2;
            this.status_p3 = this.obj_data[0].p3;
            this.status_re = this.obj_data[0].status_redeem;

            if( this.status_p1 == '1'  && this.status_p2 == '1'  && this.status_p3 == '1'  && this.status_re == '0'  ) {
              this.submit_redeem = true;
            }
            console.log(this.status_p1,this.status_p2,this.status_p3,this.status_re,this.submit_redeem);

              // this.redeem(qr);
          } else {
            alert('no data');
          }
        }, error => {
          console.log(error);
  
        });
      }
  }

  redeem(){
    if(this.submit_redeem == true) {
      this.updateAPI = "http://varpevent.com/e/2018-essilor/api/redeem/" + this.qrcodeID;
      this.api.postData(this.updateAPI)
      .subscribe(res => {
        // this.obj_redeem = res.status;
        // if(this.obj_redeem[0].api == "ok") {
          this.submit_redeem = false;
          alert('redeem success');
          this.view_redeem = false;
        // } else {
        //   alert('redeem fail');
        // }
        // let TIME_IN_MS = 7000;
        // let hideTimeout = setTimeout( () => {
        // this.navCtrl.popToRoot();
        // }, TIME_IN_MS);
      })
    } else {
      alert('redeem fail');
    }
  }


  backToscan() {
    this.view_redeem = false;
  }

  backToHome() {
    this.navCtrl.popToRoot();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad RedeemPage');
  }

}
