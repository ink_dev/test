import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { CheckpointPage } from '../checkpoint/checkpoint';
import { RedeemPage } from '../redeem/redeem';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,) {

  }

  goCheckipoint(num) {

    this.navCtrl.push(CheckpointPage,{ checkpoint: num });
  }

  goRedeem() {
    // this.options =  {
    //   preferFrontCamera:true
    // }
    
    // this.BarcodeScanner.scan(this.options).then ((barcodeData) => {

      this.navCtrl.push(RedeemPage,{ });
    // },(err) => {
    //   alert(err);
    //   console.log('error', err);
    // });
  }

}
